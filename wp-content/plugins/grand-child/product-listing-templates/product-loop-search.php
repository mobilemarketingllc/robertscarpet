<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
    <?php 
           $col_class = 'col-md-3 col-sm-4 col-xs-6';
    ?>

    
<?php while ( have_posts() ): the_post(); ?>
<?php if(get_field('swatch_image_link')) {
    
    //collection field
    $collection = get_field('collection', $post->ID);?>
    <div class="<?php echo $col_class; ?>">
    
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				<?php 
						$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');							
							
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>      
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey <?php if( get_field('brand', $post->ID)== 'COREtec' || get_field('brand', $post->ID)== 'coretec'){ echo 'sample-product-grid'; }?>">
            <h4><?php the_field('collection'); ?></h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>
			
             <?php 
             $brands = array("Karastan");
             $brand =  get_field('brand', $post->ID);
             $flooringtype = $post->post_type; 
?>

<?php if (in_array($brand, $brands) && $flooringtype == 'carpeting'){ ?>
									<form class="frmCart" name="<?php echo $sku; ?>">
									<input type="hidden" id="qty_<?php echo $sku; ?>" name="quantity" value="1" size="2" />
									<input type="hidden"  name="sku" value="<?php echo $sku; ?>" />           
									
									<?php
										$in_session = "0";
										if(!empty($_SESSION["cart_item"])) {
											$session_code_array = array_keys($_SESSION["cart_item"]);
											if(in_array($sku,$session_code_array)) {
												$in_session = "1";
											}
										}
									?>
									<!-- <input type="button" id="add_<?php echo $sku; ?>" value="Order Free Sample" class="btnAddAction cart-action" onClick = "cartAction('add','<?php echo $sku; ?>', '<?php echo $post->ID; ?>')" <?php if($in_session != "0") { ?>style="display:none" <?php } ?> />
									<input type="button" id="added_<?php echo $sku; ?>" value="Added" class="btnAdded" <?php if($in_session != "1") { ?>style="display:none" <?php } ?> /> -->

									<a href="javascript:void(0)" id="add_<?php echo $sku; ?>" target="_self" class="fl-button getcoupon-btn btnAddAction cart-action" 
									role="button" onClick = "cartAction('add','<?php echo $sku; ?>', '<?php echo $post->ID; ?>')">
										<span class="fl-button-text">
										<?php if($in_session == "0") { ?>ORDER FREE SAMPLE<?php } ?>
										<?php if($in_session == "1") { ?>SAMPLE ADDED<?php } ?>
										</span>
									</a>

									</form>
									<?php } ?>

			 <?php if( get_option('getcouponbtn') == 1){  ?>
                <a href="<?php if(get_option('getcouponreplace')==1){  echo get_option('getcouponreplaceurl').'?product_id='.$post->ID; }else{ echo '/flooring-coupon/'; } ?>" target="_self" class="fl-button getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?></span>
            </a>
            </a><br />
            <?php } ?>
            <?php if($show_financing == 1){?>
            <a href="<?php echo site_url(); ?>/flooring-financing/" target="_self" class="fl-button plp-getfinance-btn" role="button" >
                <span class="fl-button-text">GET FINANCING</span>
            </a><br />
           <?php } ?>
            <a class="link plp-view-product" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
        </div>
    </div>
    </div>
        <?php } ?>
<?php endwhile; ?>
</div>
</div>