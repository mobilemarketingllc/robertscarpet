<div class="product-detail-layout-1">
<?php
global $post;
$flooringtype = $post->post_type; 
$meta_values = get_post_meta( get_the_ID() );
$brand = $meta_values['brand'][0] ;
$sku = $meta_values['sku'][0];
$manufacturer = $meta_values['manufacturer'][0];
$image_600 = swatch_image_product(get_the_ID(),'600','400');

$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');

?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
		<div class="fl-post-content clearfix grey-back" itemprop="text">
					<div class="container">
				<div class="row">
					<div class="col-md-7 col-sm-12 product-swatch">   
						<?php 
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
							include( $dir );
						?>
					</div>
					<div class="col-md-5 col-sm-12 product-box">
						<?php					
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
							include_once( $dir );                
						?>
						<?php if(array_key_exists("parent_collection",$meta_values)){?>
							<h4><?php echo $meta_values['parent_collection'][0]; ?></h4>
						<?php } ?>
						
						<?php if(array_key_exists("collection",$meta_values)){ ?>
						<h2 class="fl-post-title" itemprop="name"><?php echo $meta_values['collection'][0]; ?></h2>
						<?php } ?>

						<?php if(array_key_exists("color",$meta_values)){ ?>
						<h1 class="fl-post-title" itemprop="name"><?php echo $meta_values['color'][0]; ?></h1>
						<?php } ?>

						<?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
						
						<div class="product-colors">
							<?php

								$collection = $meta_values['collection'][0]; 
								if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

									$familycolor = $meta_values['style'][0];
									$key = 'style';

								}else{

									$familycolor = $meta_values['collection'][0];    
									$key = 'collection';
								}	
	
								$args = array(
									'post_type'      => $flooringtype,
									'posts_per_page' => -1,
									'post_status'    => 'publish',
									'meta_query'     => array(
										array(
											'key'     => $key,
											'value'   => $familycolor,
											'compare' => '='
										),
										array(
											'key' => 'swatch_image_link',
											'value' => '',
											'compare' => '!='
											)
									)
								);										
						
								$the_query = new WP_Query( $args );
							?>
							<ul>
								<li class="color-count" style="font-size:14px;"><?php  echo $the_query ->found_posts; ?> Colors Available</li>
							</ul>
						</div>
						
						<div class="button-wrapper <?php if( get_field('brand', $post->ID)== 'COREtec' || get_field('brand', $post->ID)== 'coretec'){ echo 'sample-product-btn'; }?>">

						<?php if( get_field('brand', $post->ID)== 'COREtec' || get_field('brand', $post->ID)== 'coretec'){ ?>
							<form id="frmCart"  class="frmCart" name="<?php echo $sku; ?>">
							<input type="hidden" id="qty_<?php echo $sku; ?>" name="quantity" value="1" size="2" />
							<input type="hidden"  name="sku" value="<?php echo $sku; ?>" />           
							
							<?php
								$in_session = "0";
								if(!empty($_SESSION["cart_item"])) {
									$session_code_array = array_keys($_SESSION["cart_item"]);
									if(in_array($sku,$session_code_array)) {
										$in_session = "1";
									}
								}
							?>
							<!-- <input type="button" id="add_<?php echo $sku; ?>" value="Order Free Sample" class="btnAddAction cart-action" onClick = "cartAction('add','<?php echo $sku; ?>', '<?php echo $post->ID; ?>')" <?php if($in_session != "0") { ?>style="display:none" <?php } ?> />
							<input type="button" id="added_<?php echo $sku; ?>" value="Added" class="btnAdded" <?php if($in_session != "1") { ?>style="display:none" <?php } ?> /> -->

							<a href="javascript:void(0)" id="add_<?php echo $sku; ?>" target="_self" class="fl-button getcoupon-btn btnAddAction cart-action" 
							role="button" onClick = "cartAction('add','<?php echo $sku; ?>', '<?php echo $post->ID; ?>')">
								<span class="fl-button-text">
								<?php if($in_session == "0") { ?>ORDER FREE SAMPLE<?php } ?>
								<?php if($in_session == "1") { ?>SAMPLE ADDED<?php } ?>
								</span>
							</a>

							</form>
							<?php } ?>

						<?php if( $getcouponbtn == 1){  ?>
												<a href="<?php if( $getcouponreplace ==1){ echo $getcouponreplaceurl;}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
												<?php if($getcouponreplace ==1){ echo $getcouponreplacetext ;}else{ echo 'GET COUPON'; }?>
												</a>
						<?php } ?>
							<a href="/contact-us/" class="button contact-btn">Contact Us</a>
							
						<?php if($pdp_get_finance != 1 || $pdp_get_finance  == '' ){?>						
							<a href="<?php if($getfinancereplace ==1){ echo $getfinancereplaceurl ;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext;}else{ echo 'Get Financing'; } ?></a>	
						<?php } ?>

						<?php  roomvo_script_integration($manufacturer,$sku);?>
						
						</div>
					</div>
				</div>
					<div class="clearfix"></div>
				<?php 
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider.php';
					include( $dir );
				?>
					<div class="clearfix"></div>
				<?php
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
					include( $dir );
				?>
			</div>
			</div><!-- .fl-post-content -->
	</article>
</div>
<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>
			<script>
			jQuery(document).ready(function() {
				queryString_sta = 'action=get_sample_status&sku=<?php echo $sku;  ?>';
				jQuery.ajax({
					url: ajaxurl,
					data: queryString_sta,
					type: "POST",
					success: function(response) {
						jQuery(".btnAddAction").html(response);
					},
					error: function() {

					}
				});
			});
			</script>