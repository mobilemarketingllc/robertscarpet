<div class="product-grid swatch" itemscope itemtype="http://schema.org/ItemList">

    <div class="row product-row">
    <?php 

    $show_financing = get_option('sh_get_finance');

    if(postpercol == '4')
    {
        $col_class = 'col-md-3 col-sm-4 col-xs-6';
    }
    else
    {
        $col_class = 'col-md-4 col-sm-4';
    }
    ?>
<?php 

$brands = array("Karastan");

$K = 1;
while ( have_posts() ): the_post(); 
      //collection field
      $collection = get_field('collection', $post->ID);
      $sku = get_field('sku', $post->ID);
      $flooringtype = $post->post_type; 
?>
    <div class="<?php echo $col_class; ?>">    
    <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

    <meta itemprop="position" content="<?php echo $K;?>" />
        <?php // FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                  <?php 
                  
                   $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
							
							
					?>
            <img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey <?php if( get_field('brand', $post->ID)== 'COREtec' || get_field('brand', $post->ID)== 'coretec'){ echo 'sample-product-grid'; }?>">
            <h4><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { ?> <?php the_field('collection'); ?> <?php the_field('style'); ?> <?php } else{ ?><?php the_field('collection'); ?> <?php } ?> </h4>
            <h2 class="fl-post-grid-title" itemprop="name">
                <a href="<?php the_permalink(); ?>" itemprop="url" title="<?php the_title_attribute(); ?>"><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { echo get_field('color'); } else{ echo get_field('color'); }?></a>
            </h2>           

            <?php $manufacturer = get_field('brand_facet', $post->ID);
            
            if (in_array($manufacturer, $brands) && $flooringtype == 'carpeting') {?>
            <!-- <form name="<?php echo $sku; ?>">
            <input type="hidden" id="qty_<?php echo $sku; ?>" name="quantity" value="1" size="2" />
            <input type="hidden"  name="sku" value="<?php echo $sku; ?>" />           
             
             <?php
				// $in_session = "0";
				// if(!empty($_SESSION["cart_item"])) {
				// 	$session_code_array = array_keys($_SESSION["cart_item"]);
				//     if(in_array($sku,$session_code_array)) {
				// 		$in_session = "1";
				//     }
				// }
			?>
            
            <a href="javascript:void(0)" id="add_<?php echo $sku; ?>" target="_self" class="fl-button btnAddAction cart-action" 
            role="button" onClick = "cartAction('add','<?php echo $sku; ?>', '<?php echo $post->ID; ?>')">
                <span class="fl-button-text">
                <?php if($in_session == "0") { ?>ORDER SAMPLE<?php } ?>
                <?php if($in_session == "1") { ?>SAMPLE ADDED<?php } ?>
                </span>
            </a>

            </form> -->
            <?php } ?>

            <?php if( get_option('getcouponbtn') == 1){  ?>
                <a 
                href="<?php if(get_option('getcouponreplace')==1){ 
                    // if( get_field('brand', $post->ID)== 'Karastan'){

                    //     echo '/flooring/karastan/';

                    //  }else{
                         echo get_option('getcouponreplaceurl').'?product_id='.$post->ID;
                       //  }
                   }?>"
                target="_self" class="fl-button getcoupon-btn get-cpn-link" role="button" 
                >
                <span class="fl-button-text">
                <?php if(get_option('getcouponreplace')==1){ 

                // if( get_field('brand', $post->ID)== 'Karastan'){

                //     echo 'ON SALE NOW';
                    
                //     }else{
                    echo get_option('getcouponreplacetext');
                  //  }

                 }?>
                </span>
            </a>
            </a><br />
            <?php } ?>

           <?php if($show_financing == 1){?>
            <a href="<?php if(get_option('getfinancereplace')==1){ echo get_option('getfinancereplaceurl');}else{ echo '/flooring-financing/'; } ?>" target="_self" class="fl-button plp-getfinance-btn" role="button" >
                <span class="fl-button-text"><?php if(get_option('getfinancereplace')=='1'){ echo get_option('getfinancetext');}else{ echo 'Get Financing'; } ?></span>
            </a>
            <br />
           <?php } ?>          
          
            <a class="link plp-view-product" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>

        </div>
    </div>
    </div>
<?php  $K++; endwhile; ?>
</div>

</div>