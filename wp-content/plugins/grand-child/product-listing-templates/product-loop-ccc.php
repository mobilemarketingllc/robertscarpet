<style>
#productModal.fade{
	display:block;
}
#productModal .modal-header .modal_cls_btn{float:right;}
#productModal .modal-footer{text-align:right;}
#productModal{
	display:none;
    position: fixed;
    margin: auto;
    left: 0;
    right: 0;
    border: 0;
    background: #0000001f;
    top: 0;
    z-index: 999;
	bottom:0;
}
#productModal .modal-content {
    width: 700px;
    background: #fff;
    margin: 50px auto 0;
    padding: 20px;
}
#productModal .gform_wrapper .top_label input.medium,#productModal .gform_wrapper .top_label select.medium{
    width:100% !important;
}

</style>
<div class="product-grid swatch product-grid-ccc" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
    <?php 

    $show_financing = get_option('sh_get_finance');

    if(postpercol == '4')
    {
        $col_class = 'col-md-3 col-sm-4 col-xs-6';
    }
    else
    {
        $col_class = 'col-md-4 col-sm-4';
    }
    ?>
<?php while ( have_posts() ): the_post(); 
      //collection field
      $collection = get_field('collection', $post->ID);
?>
    <div class="<?php echo $col_class; ?>">    
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				   <?php 												
                     $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');							
							
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { ?> <?php the_field('collection'); ?> <?php the_field('style'); ?> <?php } else{ ?><?php the_field('collection'); ?> <?php } ?> </h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php  the_title(); ?></a>
            </h2>
            <p ><?php echo get_field('brand'); ?></p>
            
            <?php 
            
             $promsale =  json_decode(get_option('saleconfiginformation')); 

             //print_r($promsale)

             $sale_arr = array();

             $brand_arr = array();

             $i = 0 ;



             foreach ($promsale as $sale) {
                
                 if($sale->getCoupon == 1){

                    $brand_arr = array_merge($brand_arr,$sale->brandList);

                    $sale_end_date   =  date("d-m-Y", substr($sale->endDate, 0, 10)); 
                    $sale_start_date =  date("d-m-Y", substr($sale->startDate, 0, 10)); 

                    $sale_arr[$i]['promoCode'] = $sale->promoCode;
                    $sale_arr[$i]['name']      = $sale->name; 
                    $sale_arr[$i]['startDate'] = $sale_start_date; 
                    $sale_arr[$i]['endDate']   = $sale_end_date;   
                    $sale_arr[$i]['getCoupon'] = $sale->getCoupon;   
                    $sale_arr[$i]['brandList'] = $sale->brandList;   


                    $i++;
                 }
             }
           
            ?>

<?php if( get_field('brand', $post->ID)== 'COREtec' || get_field('brand', $post->ID)== 'coretec'){ ?>
									<form id="frmCart"  class="frmCart" name="<?php echo $sku; ?>">
									<input type="hidden" id="qty_<?php echo $sku; ?>" name="quantity" value="1" size="2" />
									<input type="hidden"  name="sku" value="<?php echo $sku; ?>" />           
									
									<?php
										$in_session = "0";
										if(!empty($_SESSION["cart_item"])) {
											$session_code_array = array_keys($_SESSION["cart_item"]);
											if(in_array($sku,$session_code_array)) {
												$in_session = "1";
											}
										}
									?>
									<!-- <input type="button" id="add_<?php echo $sku; ?>" value="Order Free Sample" class="btnAddAction cart-action" onClick = "cartAction('add','<?php echo $sku; ?>', '<?php echo $post->ID; ?>')" <?php if($in_session != "0") { ?>style="display:none" <?php } ?> />
									<input type="button" id="added_<?php echo $sku; ?>" value="Added" class="btnAdded" <?php if($in_session != "1") { ?>style="display:none" <?php } ?> /> -->

									<a href="javascript:void(0)" id="add_<?php echo $sku; ?>" target="_self" class="fl-button getcoupon-btn btnAddAction cart-action" 
									role="button" onClick = "cartAction('add','<?php echo $sku; ?>', '<?php echo $post->ID; ?>')">
										<span class="fl-button-text">
										<?php if($in_session == "0") { ?>ORDER FREE SAMPLE<?php } ?>
										<?php if($in_session == "1") { ?>SAMPLE ADDED<?php } ?>
										</span>
									</a>

									</form>
									<?php } ?>


           <?php if( get_option('getcouponbtn') == 1){  ?>
                <a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/coupon/'; } ?>" target="_self" class="fl-button getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?></span>
            </a>
            <br />
            <?php } ?>
           
            <a  href="#" class="link  enquiry_link productModalLink"  data-toggle="modal" data-product="<?php the_title(); ?>" 
										data-title="<?php the_title(); ?>" data-brand="<?php the_field('brand'); ?>"  data-product_number="<?php the_field('sku'); ?>" 
										data-product_family="<?php the_field('collection'); ?>">Request Info</a>
        </div>
    </div>
    </div>
<?php endwhile; ?>
</div>
</div>

<?php
		$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/modal-product-ccc.php';
		include( $dir );
	?>