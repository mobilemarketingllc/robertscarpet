<?php
// Variable for Product Loop listing page Column
$col_css = postpercol + 1;

$color = (get_option('api_colorcode_sandbox') != "")?get_option('api_colorcode_sandbox'):"#00247a";
$seleinformation = json_decode(get_option('saleinformation'));
$seleinformation = (object)$seleinformation;
$current_color = get_theme_mod( 'fl-accent' );
$current_hover_color = get_theme_mod( 'fl-accent-hover' );
?>
<style>
.sample-popup-main-wrap .sample-popup-wrap .cart-count .mycart-text{
    background: <?php echo $current_color ?>;
    opacity:0.90;
}
.sample-popup-main-wrap .sample-popup-wrap .cart-count .mycart-count{
    background-color: <?php echo $current_color ?>;
    opacity:1;
}
.sample-popup-main-wrap .sample-popup-wrap .middle-section .added-samples:before{
    border: 1px solid <?php echo $current_color ?>;
    color: <?php echo $current_color ?>;
}
#shopping-cart .txt-heading #btnEmpty {
    border:1px solid <?php echo $current_color ?>;
    background-color: <?php echo $current_color ?>;
}
#shopping-cart .txt-heading #btnEmpty:hover{
    color: <?php echo $current_color ?>;
    border-color: <?php echo $current_color ?>;
}

.sample-popup-main-wrap .sample-popup-wrap .bottom-section .total-free .free{
    color: <?php echo $current_color ?>;
}
a.fl-button:hover, .fl-builder-content a.fl-button:hover{
    background-color: <?php echo $current_hover_color ?>;
}
#shopping-cart {
    border: 1px solid <?php echo $current_color ?>;
    border-top: 2px solid <?php echo $current_color ?>;
}

.salebanner-slide{
	max-height:440px!important;
	margin: 0 auto;
    display: block;
}
.homepage-banner-bg{
	background-image: url(<?php  if(isset($seleinformation->background_image)){echo $seleinformation->background_image;}?>)!important;
	background-repeat: no-repeat;
}

.fl-row-content-wrap.custombg {
    //background-image: url(<?php if(isset($seleinformation->background_image)){echo $seleinformation->background_image;}?>)!important;
	background-image: url(<?php if(isset($seleinformation->background_image_landing)){echo $seleinformation->background_image_landing;}?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}
.fl-row-content-wrap.homepage {
    background-image: url(<?php if(isset($seleinformation->background_image)){echo $seleinformation->background_image;}?>)!important;
//	background-image: url(<?php if(isset($seleinformation->background_image_landing)){echo $seleinformation->background_image_landing;}?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}

img.salebanner.fl-slide-photo-img {
    max-width: 500px;
}

.banner-mobile{
	display: none;
	text-align:center;
}

@media(max-width:1080px){
.fl-page-header-primary .fl-logo-img{
max-width: 80%;
}
}

@media(max-width:992px){
.fl-page-header-primary .fl-logo-img{
max-width: 50%;
}
.banner-below-heading-n-text h2 span{
font-size: 25px;
line-height: 28px;
}
.banner-deskop{
display: none;
}
.banner-mobile{
	display: block;
}
}
</style>
